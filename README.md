# 🏫 项目介绍

霍格沃兹测试学院 测开17期 课程资料


# 🌐 学院论坛

[测试人论坛](https://ceshiren.com/)


# 📃 直播课贴子

## pytest
- [pytest测试实战1](pytest/pytest测试实战1课程贴.md)
- [pytest测试实战2](pytest/pytest测试实战2课程贴.md)


# 💻 课程代码

[HogwartsSDET17](https://github.com/ceshiren/HogwartsSDET17)


# 🗒️ 课程 PPT

[PPT 汇总](ppt_catalog.md)