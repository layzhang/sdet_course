# pytest 测试实战（二）


## 参考链接
- pytest 用例信赖插件：https://pytest-dependency.readthedocs.io/en/latest/usage.html#basic-usage

- pytest hook：https://docs.pytest.org/en/latest/_modules/_pytest/hookspec.html

- 如何打包：https://packaging.python.org/tutorials/packaging-projects/

- 如何打包，发布：https://packaging.python.org/guides/distributing-packages-using-setuptools/


## 课程价值
- Fixture 高级用法
- pytest 常用插件
- hook函数
- Allure生成测试报告


## 大纲
- Fixture 高级用法
- pytest 常用插件
- hook函数
- Allure生成测试报告


## PPT
https://pdf.ceshiren.com/ck17/pytest02


## Fixture的作用
- Fixture是在测试函数运行前后，由pytest执行的外壳函数，代码可以定制，满足多变的测试需求，功能包括：
    - 定义传入测试中的数据集
    - 配置测试前系统的初始状态
    - 为批量测试提供数据源等
- Fixture 是pytest 用于将测试前后进行预备，清理工作的代码分离出核心测试逻辑的一种机制


## Fixture 用法
- Fixture 是为了测试⽤例的执⾏，初始化⼀些数据和⽅法
    1、类似 setUp, tearDown 功能，但⽐ setUp, tearDown 更灵活
    2、直接通过函数名字调⽤或使用装饰器@pytest.mark.usefixtures(‘test1’)
    3、允许使用多个Fixture
    4、使用 autouse 自动应用，如果要返回值，需要传fixture函数名
    5、作用域（session>module>class>function）
    6、也可以提供测试数据，实现参数化的功能
    7、Fixture也可以调用Fixture
    --setup-show 回溯 fixture 的执行过程


## conftest.py 用法
- 数据共享的文件，名字是固定的，不能修改
- 可以存放fixture , hook 函数
- 就近生效（如果不在同一个文件夹下，离测试文件最近的conftest.py 生效）
- 当前目录一定要有__init__.py 文件，也就是要创建一个包


## pytest.ini 的用法
参考 `pytest --help`
Demo:
```ini
[pytest]
markers = login
    search
python_files = check_* test_*
python_functions = check_* test_*
addopts = -vs --alluredir=./result
```


## pytest 常用的插件
```bash
pip install pytest-ordering      # 控制用例的执行顺序
pip install pytest-dependency    # 控制用例的依赖关系
pip install pytest-xdist         # 分布式并发执行测试用例
pip install pytest-rerunfailures # 失败重跑
pip install pytest-assume        # 多重较验
pip install pytest-random-order  # 用例随机执行
pip intall  pytest-html          # 测试报告
```


## 测试用例基本原则
不要让 case 有顺序
不要让测试用例有依赖
如果你无法做到，可以临时性的用插件解决
课间作业
1、 将作业改造成 fixture 实现setup，teardown
2、将数据提取出来放在conftest.py 文件中
3、添加 addopts 参数到pytest.ini 文件中，定义默认运行 生成alluredir


## pytest 插件
- 内置plugin（hook函数）:
从代码内部的_pytest目录加载
- 外部插件（第三方插件）：
    - 通过 setuptools entry points 机制发现的第三方插件
    - pip install 安装的插件
    （https://docs.pytest.org/en/latest/plugins.html） 5
- conftest.py存放的本地插件：
    自动模块发现机制
- `pytest --trace-config` 查看当前pytest中所有的plugin（带有hook方法的文件）


## 打包
安装软件：
```bash
setuptools
pip install wheel
```


## 打包命令
```bash
python setup.py sdist bdist_wheel
```


## setup.py 配置
```python
from setuptools import setup
setup(
    name='pytest_encode',
    url='https://github.com/xxx/pytest-encode',
    version='1.0',
    author="xixi",
    author_email='418974188@qq.com',
    description='set your encoding and logger',
    long_description='Show Chinese for your mark.parametrize(). Define logger variable for getting your log',
    classifiers=[# 分类索引 ，pip 对所属包的分类
        'Framework :: Pytest',
        'Programming Language :: Python',
        'Topic :: Software Development :: Testing',
        'Programming Language :: Python :: 3.8',
    ],
    license='proprietary',
    packages=['pytest_encode'],
    keywords=[
        'pytest', 'py.test', 'pytest_encode',
    ],

    # 需要安装的依赖
    install_requires=[
        'pytest'
    ],
    # 入口模块 或者入口函数
    entry_points={
        'pytest11': [
            'pytest-encode = pytest_encode',
        ]
    },
    zip_safe=False
)
```


## allure 用法
- 为测试用例分类：
```python
@allure.feature("name")
@allure.story("name")
```
- 为测试 用例加标题：
```python
@allure.title()
```
- 生成测试报告

    Allure2 解析过程：
    1. 安装 allure2
    2. Allure help  帮助文档 
    3. 生成 allure 测试结果 ：pytest --alluredir=./report/
    4. 展示报告：allure serve ./report
    5. 生成最终版本的报告：   allure generate ./report
    在本地搭建一个网站服务（例如：Django）
    python manage.py runserver (http://127.0.0.1:8000/)


## 参考代码
https://github.com/ceshiren/HogwartsSDET17


## 作业
1. 将上节课的作业 ，改造成fixture (conftest.py)
2. 自己写个hook，打包，安装到新的虚拟环境下使用