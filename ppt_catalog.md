# Python 测试开发课程 PPT

> 备注: 只有部分课程提供了 PPT。
> 使用指南: 在 PPT 播放界面，将鼠标移至左侧可弹出侧边栏。按下 `?` 查看快捷键说明


## Linux 命令 & Bash 脚本

https://pdf.ceshiren.com/lbppt/linux/linux01_05


## 测试框架

### 录播课

https://pdf.ceshiren.com/ck16/unittest

https://pdf.ceshiren.com/ck16/pytest

https://pdf.ceshiren.com/ck16/参数化

https://pdf.ceshiren.com/ck16/allure

### 直播课

https://pdf.ceshiren.com/ck17/pytest01

https://pdf.ceshiren.com/ck17/pytest02


## Web 自动化测试

https://pdf.ceshiren.com/lbppt/selenium/selenium01_05

https://pdf.ceshiren.com/lbppt/selenium/selenium06_09

https://pdf.ceshiren.com/lbppt/selenium/selenium10_12


## APP 自动化测试

### 录播课

https://pdf.ceshiren.com/lbppt/appium/01-05appium

https://pdf.ceshiren.com/lbppt/appium/6-10appium

https://pdf.ceshiren.com/lbppt/appium/11-13appium

### 直播课

https://pdf.ceshiren.com/ck17/appium1

https://pdf.ceshiren.com/ck17/appium2


## UI 自动化测试框架

https://pdf.ceshiren.com/lbppt/ui自动化测试框架/交互api模拟器控制


## 持续集成

https://pdf.ceshiren.com/ck17/jenkins02/0418_UI自动化测试与持续集成体系建设实战


## 持续交付 / DevOps

https://pdf.ceshiren.com/ck17/jenkins-cd/jenkins-cd