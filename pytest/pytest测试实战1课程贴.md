# python pytest 测试实战（一）


## 课程价值
- pytest 安装
- PyCharm 配置运行pytest测试
- Git 配置
- 掌握参数化
- 理解 pytest 框架结构
- 理解 pytest Fixture 用法


## 大纲
- pytest 介绍与安装
- pytest 常用执行参数
- pytest 框架结构
- pytest 参数化与数据驱动
- pytest fixture


## PPT
https://pdf.ceshiren.com/ck17/pytest01


## git 配置
[Git](最全的git命令.md)


## 参考链接
- 命名规范: https://zh-google-styleguide.readthedocs.io/en/latest/google-python-styleguide/python_style_rules/#id16 35
- yaml：https://yaml.org/spec/1.2/spec.html 13


## pytest 规则
- 测试用例命名规范
- 文件要在test_开头，或者_test结尾
- 类要以Test开头，方法要以test_开头


## 常用参数
```bash
pytest --collect-only #只收集用例
pytest -k "add"       #匹配所有名称中包含add的用例（'add or div' 'TestClass'）
pytest -m mark标签名   #标记
pytest --junitxml=./result.xml   #生成执行结果文件
pytest --setup-show              #回溯fixture的执行过程
```


## 参考代码
https://github.com/ceshiren/HogwartsSDET17


## 作业
- 课后作业
    1. 补全计算器（加法 除法）的测试用例
    2. 使用参数化完成测试用例的自动生成
    3. 在调用测试方法之前打印【开始计算】，在调用测试方法之后打印【计算结束】
- 注意：
    - 使用等价类，边界值，因果图等设计测试用例
    - 测试用例中添加断言，验证结果
    - 灵活使用 setup(), teardown() , setup_class(), teardown_class()